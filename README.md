## field_delegate

class `Field` is an abstract mutable field delegate that can be used as an interface to access different data sources.

See example usage in [shared_preferences_field_delegate](https://pub.dev/packages/shared_preferences_field_delegate)

## Usage

The class `Field` contains value getter, setter and a stream of changes

```dart
Field<int> intField;
int value = intField.get();
Future<void> result = intField.set(1);
Stream<int> changes = intField.onChanged;
```

You can map a value of `Field` to another type with `Field.map` method

```dart
Field<int> intField;
Field<String> mappedField = Field.map<int, String>(
  source: intField,
  mapToSource: (value) => int.parse(value),
  mapFromSource: (value) => value.toString(),
);
```

If you have a nullable `Field`, you can map it to a non-nullable `Field` with a default value. Use `Field.notNullable` function for it

```dart
Field<int?> intNullableField;
Field<int> intNotNullableField = Field.notNullable(
  source: nullableFiled,
  defaultValue: 0,
);
```