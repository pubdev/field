part 'mapped_field.dart';

part 'not_nullable_field.dart';

/// An interface that represents value holder.
/// Exposes value getter, setter and stream of changes.
///
/// Generic type [T] may be nullable, e.g. `int?`. This way it allows to simulate nullable values.
///
/// Nullable [Field] can map to non-nullable [Field] via [Field.notNullable] method.
/// Refer to method documentation for more info.
///
/// It is possible to map [Field] to another type using [Field.map] method.
/// Refer to method documentation for more info.
abstract class Field<T> {
  /// Returns a stream of changes of [Field]'s value.
  ///
  /// Emits when [set] method is being called.
  Stream<T> get onChanged;

  /// Returns [Field]'s value.
  T get();

  /// Allows to set a new value to this [Field].
  Future<void> set(T value);

  /// Returns an instance of [Field] with another type mapped using mappers provided as parameters.
  ///
  /// [source] - [Field] with a generic type that needs to be mapped to another type.
  /// [mapToSource] - function that maps value to source [Field].
  /// [mapFromSource] - function that maps value from source [Field].
  ///
  /// Example:
  /// ```
  /// Field<String> stringField = ...
  /// final intField = Field.map<int, String>(
  ///   source: stringField,
  ///   mapToSource: (value) => int.parse(value),
  ///   mapFromSource: (value) => value.toString(),
  /// );
  /// int value = intField.get(); // string mapped to int
  /// ```
  static Field<O> map<I, O>({
    required Field<I> source,
    required ValueConverter<O, I> mapToSource,
    required ValueConverter<I, O> mapFromSource,
  }) =>
      _MappedField(
        source,
        mapToSource,
        mapFromSource,
      );

  /// Returns an instance of non-nullable [Field], i.e. [Field] that always has either actual or default value.
  ///
  /// Example:
  /// ```
  /// Field<int?> nullableField = ...
  /// final notNullableField = Field.notNullable(
  ///   source: nullableField,
  ///   defaultValue: 0,
  /// );
  /// nullableField.get() // returns either `value` or `defaultValue` if Field instance's `value` is null
  /// ```
  static Field<T> notNullable<T>({
    required Field<T?> source,
    required T defaultValue,
  }) =>
      _NotNullableField(
        source,
        defaultValue,
      );
}
