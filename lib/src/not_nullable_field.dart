part of 'field.dart';

class _NotNullableField<T> extends Field<T> {
  final Field<T?> _nullableField;
  final T _defaultValue;

  _NotNullableField(this._nullableField, this._defaultValue);

  @override
  Stream<T> get onChanged =>
      _nullableField.onChanged.map((event) => event ?? _defaultValue);

  @override
  T get() => _nullableField.get() ?? _defaultValue;

  @override
  Future<void> set(T value) => _nullableField.set(value);
}
