part of 'field.dart';

typedef ValueConverter<TIn, TOut> = TOut Function(TIn value);

class _MappedField<TIn, TOut> extends Field<TOut> {
  final Field<TIn> _parent;
  final ValueConverter<TOut, TIn> mapToParent;
  final ValueConverter<TIn, TOut> mapFromParent;

  _MappedField(
    this._parent,
    this.mapToParent,
    this.mapFromParent,
  );

  @override
  TOut get() => mapFromParent(_parent.get());

  @override
  Future<void> set(TOut value) => _parent.set(mapToParent(value));

  @override
  Stream<TOut> get onChanged => _parent.onChanged.map(mapFromParent);
}
