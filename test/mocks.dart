import 'dart:async';

import 'package:field_delegate/field_delegate.dart';

class FakeField<T> extends Field<T> {
  final _streamController = StreamController<T>.broadcast();
  T _value;

  FakeField(this._value);

  @override
  Stream<T> get onChanged => _streamController.stream;

  @override
  T get() => _value;

  @override
  Future<void> set(T value) async {
    _streamController.add(value);
    _value = value;
  }
}
