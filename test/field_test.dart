import 'package:field_delegate/field_delegate.dart';
import 'package:test/test.dart';

import 'mocks.dart';

main() {
  test('check mapping type of Field with Field.map function', () {
    final sourceField = FakeField<int>(1);
    final mappedField = Field.map<int, String>(
      source: sourceField,
      mapToSource: (value) => int.parse(value),
      mapFromSource: (value) => value.toString(),
    );
    expect(
      mappedField.get(),
      '1',
      reason: 'mapped Field contains invalid value',
    );
    expect(
      sourceField.get(),
      1,
      reason: 'source Field contains invalid value',
    );
  });
  test('check creation not nullable Field with Field.notNullable function', () {
    final nullableField = FakeField<int?>(null);
    const defaultValue = 0;
    final notNullableField = Field.notNullable(
      source: nullableField,
      defaultValue: defaultValue,
    );
    expect(
      notNullableField.get(),
      defaultValue,
      reason: 'notNullableField have invalid default value',
    );
    nullableField.set(1);
    expect(
      notNullableField.get(),
      1,
      reason: 'notNullableField have invalid value',
    );
    nullableField.set(null);
    expect(
      notNullableField.get(),
      defaultValue,
      reason:
          'notNullableField have invalid default value after set null to nullableField',
    );
  });
}
